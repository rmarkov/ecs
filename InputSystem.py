from InputComponent import InputComponent
from MoveComponent import MoveComponent
from myecs.System import System

class InputSystem(System):
    def filter(self, entity):
        return entity.has_component(MoveComponent) \
               and entity.has_component(InputComponent)

    def on_update(self, entities):
        for entity in entities:
            inputComponent = entity.get_component(InputComponent)
            moveComponent = entity.get_component(MoveComponent)
            if inputComponent.command == 'UP':
                moveComponent.set_move_vector([0, 1])
            if inputComponent.command == 'DOWN':
                moveComponent.set_move_vector([0, -1])
            if inputComponent.command == 'LEFT':
                moveComponent.set_move_vector([-1, 0])
            if inputComponent.command == 'RIGHT':
                moveComponent.set_move_vector([1, 0])
            inputComponent.command = None
