from myecs.Component import Component


class FieldEnvironmentComponent(Component):
    env_type: str

    def set_type(self, env_type):
        self.env_type = env_type
        return self

    def get_type(self):
        return self.env_type
