from myecs.Component import Component


class LightComponent(Component):
    light: float = 0
    light_total: float = 0

    def set_light(self, light: float):
        self.light = light
        return self

