from entities.EnvironmentEntity import EnvironmentEntity
from myecs.Component import Component


class EnvironmentEntityComponent(Component):
    entity: EnvironmentEntity = None

    def set_environment(self, environment: EnvironmentEntity):
        self.entity = environment
        return self

