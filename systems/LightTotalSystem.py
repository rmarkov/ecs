from components.EnviromentEntityComponent import EnvironmentEntityComponent
from components.LightComponent import LightComponent
from myecs.System import System

class LightTotalSystem(System):
    def filter(self, entity):
        return entity.has_component(LightComponent)

    def on_update(self, entities):
        for entity in entities:
            lightComponent = entity.get_component(LightComponent)
            environmentEntityComponent = entity.get_component(EnvironmentEntityComponent)
            light_total = lightComponent.light
            if environmentEntityComponent:
                env_light = environmentEntityComponent.entity.get_component(LightComponent).light
                light_total = min(1, light_total + env_light)
            lightComponent.light_total = light_total


