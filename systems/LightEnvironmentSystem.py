from components.EnviromentEntityComponent import EnvironmentEntityComponent
from components.LightComponent import LightComponent
from myecs.System import System

class LightEnvironmentSystem(System):
    def filter(self, entity):
        return entity.has_component(EnvironmentEntityComponent)

    def on_update(self, entities):
        for entity in entities:
            environmentEntityComponent = entity.get_component(EnvironmentEntityComponent)
            lightComponent = environmentEntityComponent.entity.get_component(LightComponent)
            lightComponent.set_light(lightComponent.light + 0.1)
