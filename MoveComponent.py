from myecs.Component import Component


class MoveComponent(Component):
    def set_move_vector(self, vector):
        self.vector = vector
        return self

    def get_move_vector(self):
        return self.vector

