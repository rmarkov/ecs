from FieldEnvironmentComponent import FieldEnvironmentComponent
from RenderComponent import RenderComponent
from myecs.System import System

def render(entity):
    fieldEnvironmentComponent = entity.get_component(FieldEnvironmentComponent)
    if fieldEnvironmentComponent:
        env_type = fieldEnvironmentComponent.get_type()
        if env_type == 'room':
            return 'Вы находитесь в комнате'
        if env_type == 'forest':
            return 'Вы находитесь в лесу'
    return ''


class RenderSystem(System):
    def filter(self, entity):
        return entity.has_component(RenderComponent)

    def on_update(self, entities):
        for entity in entities:
            renderComponent = entity.get_component(RenderComponent)
            renderComponent.set_render_text(render(entity))

