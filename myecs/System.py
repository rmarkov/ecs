class System:

    def __init__(self):
        self.entities = []

    def add_entities(self, entities):
        self.entities = entities

    def filter(self, entity):
        return True

    def on_update(self, entities):
        print(entities)

    def on_init(self, entities):
        print(entities, 'on init')

    def get_filtered_entities(self):
        entities = list(filter(self.filter, self.entities))
        return entities

    def call_on_update(self):
        entities = self.get_filtered_entities()
        self.on_update(entities)

    def call_on_init(self):
        entities = self.get_filtered_entities()
        self.on_init(entities)
