class World:

    def __init__(self):
        self.entities = []
        self.systems = []

    def add_entity(self, entity):
        self.entities.append(entity)

    def add_system(self, system):
        self.systems.append(system)
        system.add_entities(self.entities)
        system.call_on_init()

    def update(self):
        for system in self.systems:
            system.call_on_update()

