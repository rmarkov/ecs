class Entity:

    def __init__(self):
        self.components = []

    def add_component(self, component):
        self.components.append(component)

    def has_component(self, component_class):
        return bool(self.get_component(component_class))

    def get_component(self, component_class):
        for component in self.components:
            if isinstance(component, component_class):
                return component
        return None
