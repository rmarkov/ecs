from myecs.Component import Component


class RenderComponent(Component):
    render_text: str

    def set_render_text(self, render_text):
        self.render_text = render_text
        return self

    def get_render_text(self):
        return self.render_text
