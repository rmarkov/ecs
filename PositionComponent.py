from myecs.Component import Component


class PositionComponent(Component):
    def set_x(self, x):
        self.x = x
        return self
    def set_y(self, y):
        self.y = y
        return self
