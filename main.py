from FieldEnvironmentComponent import FieldEnvironmentComponent
from InputComponent import InputComponent
from InputSystem import InputSystem
from MoveComponent import MoveComponent
from MoveSystem import MoveSystem
from PlayerComponent import PlayerComponent
from RenderComponent import RenderComponent
from PositionComponent import PositionComponent
from RenderSystem import RenderSystem
from components.EnviromentEntityComponent import EnvironmentEntityComponent
from components.LightComponent import LightComponent
from entities.EnvironmentEntity import EnvironmentEntity
from myecs.Entity import Entity
from myecs.World import World
from systems.LightEnvironmentSystem import LightEnvironmentSystem
from systems.LightTotalSystem import LightTotalSystem


def test():
    world = World()

    environmentEntity = EnvironmentEntity()

    player = Entity()
    positionComponent = PositionComponent().set_x(0).set_y(0)
    player.add_component(positionComponent)
    moveComponent = MoveComponent().set_move_vector([0, 0])
    player.add_component(moveComponent)
    playerComponent = PlayerComponent()
    player.add_component(playerComponent)
    inputComponent = InputComponent()
    player.add_component(inputComponent)
    world.add_entity(player)

    field = Entity()
    positionComponent = PositionComponent().set_x(0).set_y(1)
    field.add_component(positionComponent)

    lightComponent = LightComponent().set_light(0.4)
    field.add_component(lightComponent)

    entityComponent = EnvironmentEntityComponent().set_environment(environmentEntity)
    field.add_component(entityComponent)

    fieldEnvironmentComponent = FieldEnvironmentComponent().set_type('room')
    field.add_component(fieldEnvironmentComponent)

    renderComponent = RenderComponent()
    field.add_component(renderComponent)
    world.add_entity(field)

    field1 = Entity()
    positionComponent = PositionComponent().set_x(0).set_y(2)
    field1.add_component(positionComponent)

    fieldEnvironmentComponent = FieldEnvironmentComponent().set_type('forest')
    field1.add_component(fieldEnvironmentComponent)

    renderComponent = RenderComponent()
    field1.add_component(renderComponent)
    field1.add_component(LightComponent().set_light(0.5))
    world.add_entity(field1)

    inputSystem = InputSystem()
    world.add_system(inputSystem)

    moveSystem = MoveSystem()
    world.add_system(moveSystem)

    renderSystem = RenderSystem()
    world.add_system(renderSystem)

    lightTotalSystem = LightTotalSystem()
    world.add_system(lightTotalSystem)

    lightEnvironmentSystem = LightEnvironmentSystem()
    world.add_system(lightEnvironmentSystem)

    world.update()

    print(field.get_component(RenderComponent).get_render_text())
    print(field1.get_component(RenderComponent).get_render_text())

    player.get_component(InputComponent).command = 'UP'
    world.update()
    print(player.get_component(PositionComponent).x, player.get_component(PositionComponent).y)

    print(field1.get_component(LightComponent).light_total)
    world.update()
    print(field1.get_component(LightComponent).light_total)
    world.update()
    print(field1.get_component(LightComponent).light_total)
    world.update()
    print(field1.get_component(LightComponent).light_total)
    world.update()
    print(field1.get_component(LightComponent).light_total)
    world.update()

test()
