from MoveComponent import MoveComponent
from PositionComponent import PositionComponent
from myecs.System import System

class MoveSystem(System):
    def filter(self, entity):
        return entity.has_component(MoveComponent) and entity.has_component(PositionComponent)

    def on_update(self, entities):
        for entity in entities:
            moveComponent = entity.get_component(MoveComponent)
            vector = moveComponent.get_move_vector()
            if vector[0] == 0 and vector[1] == 0:
                continue
            positionComponent = entity.get_component(PositionComponent)
            x = positionComponent.x + vector[0]
            y = positionComponent.y + vector[1]
            positionComponent.set_x(x)
            positionComponent.set_y(y)
            moveComponent.set_move_vector([0, 0])

