from components.LightComponent import LightComponent
from myecs.Entity import Entity


class EnvironmentEntity(Entity):
    def __init__(self):
        super().__init__()
        lightComponent = LightComponent().set_light(0)
        self.add_component(lightComponent)
